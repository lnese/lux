from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)
    import_href = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.vin


class Salesperson(models.Model):
    first_name = models.CharField(max_length=200, null=False)
    last_name = models.CharField(max_length=200, null=False)
    employee_id = models.CharField(max_length=200, unique=True, null=False)

    def __str__(self):
        return f"{self.employee_id} - {self.last_name}, {self.first_name}"

    class Meta:
        ordering = ("employee_id", "last_name", "first_name")

    def get_api_url(self):
        return reverse("show_salesperson", kwargs={"pk": self.pk})


class Customer(models.Model):
    first_name = models.CharField(max_length=200, null=False)
    last_name = models.CharField(max_length=200, null=False)
    address = models.TextField(null=False, blank=False)
    phone_number = models.CharField(
        max_length=200, null=False, blank=False, unique=True
    )

    def __str__(self):
        return (
            f"{self.last_name}, {self.first_name}, {self.phone_number}, {self.address} "
        )

    class Meta:
        ordering = ("last_name", "first_name", "phone_number", "address")

    def get_api_url(self):
        return reverse("show_customer", kwargs={"pk": self.pk})


class Sale(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sales",
        on_delete=models.CASCADE,
    )
    salesperson = models.ForeignKey(
        Salesperson,
        related_name="sales",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="sales",
        on_delete=models.CASCADE,
    )

    price = models.PositiveIntegerField(null=False)

    def __str__(self):
        return f"{self.automobile}, {self.customer}, {self.salesperson}, {self.price} "

    class Meta:
        ordering = ("automobile", "customer", "salesperson", "price")

    def get_api_url(self):
        return reverse("show_sale", kwargs={"pk": self.pk})
