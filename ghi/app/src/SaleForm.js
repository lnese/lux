import React, { useEffect, useState } from 'react';

function SaleForm() {
    const [automobiles, setAutomobiles] = useState([]);
    const [automobile, setAutomobile] = useState('');
    const [salespeople, setSalespeople] = useState([]);
    const [salesperson, setSalesperson] = useState('');
    const [customers, setCustomers] = useState([])
    const [customer, setCustomer] = useState('');
    const [price, setPrice] = useState('');
    const [automobileIdData, setAutomobileIdData]= useState('');


    const fetchDataAutomobiles = async () => {
        const url = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos);
        }
    }
    useEffect(() => {
        fetchDataAutomobiles();
    }, []);
    const getUnsoldCars = () => {
        return automobiles.filter(unsold => unsold.sold === false)
    }
    const fetchDataSalesPeople = async () => {
        const url = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setSalespeople(data.salespeople);
        }
    }
    useEffect(() => {
        fetchDataSalesPeople();
    }, []);

    const fetchDataCustomers = async () => {
        const url = 'http://localhost:8090/api/customers/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers);
        }
    }
    useEffect(() => {
        fetchDataCustomers();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.automobile = automobile;
        data.salesperson = salesperson;
        data.customer = customer;
        data.price = price;

        const saleUrl = 'http://localhost:8090/api/sales/';
        console.log(data)
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const responseSale = await fetch(saleUrl, fetchConfig);
        if (responseSale.ok) {
            const newSale = await responseSale.json();
            console.log(newSale);
            setAutomobileIdData(automobile);
            setAutomobile('');
            setSalesperson('');
            setCustomer('');
            setPrice('');
            handleSubmitUpdate(event)
        }
        return window.location.reload();
    }

    const handleSubmitUpdate = async (event) => {
        event.preventDefault();
        const automobileIdData = automobile;
        console.log(automobileIdData)
        const autosUrl = `http://localhost:8100/api/automobiles/${automobileIdData}/`;
        const fetchAutoConfig = {
            method: "PUT",
            body: JSON.stringify({ sold: true }),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const responseAfterPatch = await fetch(autosUrl, fetchAutoConfig);
        if (responseAfterPatch.ok) {
            const updatedAutoSoldData = await responseAfterPatch.json();
            console.log(updatedAutoSoldData);
        }
    }
    const handleAutomobileChange = (event) => {
        const value = event.target.value;
        setAutomobile(value);
    }
    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    }
    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }
    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    }
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new sale</h1>
                    <form onSubmit={handleSubmit} id="create-sale-form">
                        <div className="mb-3">
                            <select value={automobile} onChange={handleAutomobileChange} placeholder="Automobile" required name="automobile" id="automobile" className="form-select">
                                <option value="">Choose an Automobile</option>
                                {getUnsoldCars().map(automobile => {
                                    return (
                                        <option key={automobile.href} value={automobile.vin}>
                                            Color: {automobile.color}, Year: {automobile.year}, VIN: {automobile.vin}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select value={salesperson} onChange={handleSalespersonChange} placeholder="Salesperson" required name="salesperson" id="salesperson" className="form-select">
                                <option value="">Choose a Salesperson</option>
                                {salespeople.map(salesperson => {
                                    return (
                                        <option key={salesperson.href} value={salesperson.id}>
                                            Employee ID: {salesperson.employee_id}, First Name: {salesperson.first_name}, Last Name: {salesperson.last_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select value={customer} onChange={handleCustomerChange} placeholder="Customer" required name="customer" id="customer" className="form-select">
                                <option value="">Choose a Customer</option>
                                {customers.map(customer => {
                                    return (
                                        <option key={customer.href} value={customer.id}>
                                            First Name: {customer.first_name}, Last Name: {customer.last_name}, Phone Number: {customer.phone_number}, Address: {customer.address}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={price} onChange={handlePriceChange} placeholder="Price" required type="text" name="price" id="price" className="form-control" />
                            <label htmlFor="price">Price (ex: 123456)</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}
export default SaleForm;
