import React, { useState, useEffect } from "react";

function VehicleModelList(props) {
    const [models, setVehicleModels] = useState([]);

    async function fetchVehicleModels() {
        const response = await fetch('http://localhost:8100/api/models/');
        if (response.ok) {
            const data = await response.json();
            setVehicleModels(data.models);
        }
    }

    useEffect(() => {
        fetchVehicleModels();
    }, [])

    async function deleteVehicleModel(e, id) {
        e.preventDefault();

        const response = await fetch(`http://localhost:8100/api/models/${id}`, { method: 'DELETE' });

        if (response.ok) {
            fetchVehicleModels();
        }
    }

    return (
        <>
        <h1>Vehicle Models:</h1>
        <table className="table table-hover">
            <thead>
                <tr>
                    <th>Picture</th>
                    <th>Manufacturer</th>
                    <th>Model</th>
                </tr>
            </thead>
            <tbody>
                {models.map(model => {
                    return (
                        <tr key={model.href}>
                            <td><img src={model.picture_url} width="200px" height="200px" /></td>
                            <td>{model.manufacturer.name}</td>
                            <td>{model.name}</td>
                            <td>
                                <button type="button" onClick={(e) => deleteVehicleModel(e, model.id)} className="btn btn-danger">Delete</button>
                            </td>
                        </tr>
                    );
                })}
            </tbody >
        </table >
        </>
    );
}
export default VehicleModelList;
