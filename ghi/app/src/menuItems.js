export const menuItems = [
  {
    title: 'Home',
    url: '/',
  },
  {
    title: 'Services',
    url: '/services',
    submenu: [
      {
        title: 'Technicians',
        url: '/technicians',
        submenu: [
          {
            title: 'Technicians List',
            url: '/technicians',
          },
          {
            title: 'Service History',
            url: '/technicians/',
          },
          {
            title: 'New Technician',
            url: '/technicians/new',
          },
        ]
      },
      {
        title: 'Appointments',
        url: '/appointments',
        submenu: [
          {
            title: 'Appointments List',
            url: '/appointments',
          },
          {
            title: 'New Appointment',
            url: '/appointments/new',
          },
          {
            title: 'Appointment History',
            url: '/appointments',
          },
        ]
      },
    ],
  },
  {
    title: 'Sales',
    url: '/sales',
    submenu: [
      {
        title: 'Salespeople Info',
        url: '/salespeople',
        submenu: [
          {
            title: 'Salespeople List',
            url: '/salespeople',
          },
          {
            title: 'Salesperson History',
            url: '/salespeople',
          },
          {
            title: 'New Salesperson',
            url: '/salespeople/new',
          },
        ]
      },
      {
        title: 'Sales',
        url: '/sales',
        submenu: [
          {
            title: 'Sales List',
            url: '/sales',
          },
          {
            title: 'New Sale',
            url: '/sales/new',
          },
        ]
      },
      {
        title: 'Customers',
        url: '/customers',
        submenu: [
          {
            title: 'Customers List',
            url: '/customers',
          },
          {
            title: 'New Customer',
            url: '/customers/new',
          },
        ]
      }
    ],
  },
  {
    title: 'Inventory',
    url: '/inventory',
    submenu: [
      {
        title: 'Manufacturers',
        url: '/manufacturers',
        submenu: [
          {
            title: 'Manufacturers List',
            url: '/manufacturers',
          },
          {
            title: 'New Manufacturer',
            url: '/manufacturers/new',
          },
        ]
      },
      {
        title: 'Vehicle Models',
        url: '/models',
        submenu: [
          {
            title: 'Vehicle Model List',
            url: '/models',
          },
          {
            title: 'New Vehicle Model',
            url: '/models/new',
          },
        ]
      },
      {
        title: 'Automobiles',
        url: '/automobiles',
        submenu: [
          {
            title: 'Automobiles List',
            url: '/automobiles',
          },
          {
            title: 'New Automobile',
            url: '/automobiles',
          },
        ]
      }
    ],
  },
];
