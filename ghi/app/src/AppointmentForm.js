import React, { useState, useEffect } from 'react';

function AppointmentForm() {
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.date_time = date_time;
        data.reason = reason;
        data.vin = vin;
        data.customer = customer;
        data.technician = technician;

        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            setDateTime('');
            setReason('');
            setVin('');
            setCustomer('');
            setTechnician('');
        } else {
            console.log(response)
        }
    }

    const [date_time, setDateTime] = useState('');
    const handleDateTimeChange = (event) => {
        const value = event.target.value;
        setDateTime(value);
    }
    const [reason, setReason] = useState('');
    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    }
    const [vin, setVin] = useState('');
    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }
    const [customer, setCustomer] = useState('');
    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }
    const [technicians, setTechnicians] = useState([]);
    function techniciansChanged(event) {
        setTechnicians(event.target.value);
    }
    const [technician, setTechnician] = useState('');
    function technicianChanged(event) {
        const value = event.target.value
        setTechnician(value);
    }

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/';

        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setTechnicians(data.technicians);
        }
      }

      useEffect(() => {
        fetchData();
      }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a Service Appointment</h1>
            <form onSubmit={handleSubmit} id="create-shoe-form">
              <div className="form-floating mb-3">
                <input onChange={handleVinChange} value={vin} required type="text" name="vin" id="vin" className="form-control"/>
                <label htmlFor="vin">Automobile VIN</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCustomerChange} value={customer}  required type="text" name="customer" id="customer" className="form-control"/>
                <label htmlFor="customer">Customer Full Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleReasonChange} value={reason}  required type="text" name="reason" id="reason" className="form-control"/>
                <label htmlFor="reason">Reason</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleDateTimeChange} value={date_time} required type="datetime-local" name="date_time" id="date_time" className="form-control"/>
                <label htmlFor="date_time">Date and Time</label>
              </div>
              <div className="mb-3">
                <select value={technician} onChange={technicianChanged} name="technician" id="technician" className="form-select">
                  <option value="">Choose a Technician</option>
                  {technicians.map(technician => {
                    return (
                      <option key={technician.id} value={technician.id}>
                        {technician.first_name} { technician.last_name }
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}
export default AppointmentForm;
