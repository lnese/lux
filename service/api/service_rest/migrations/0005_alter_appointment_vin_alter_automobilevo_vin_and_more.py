# Generated by Django 4.0.3 on 2023-06-09 16:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0004_appointment_vip'),
    ]

    operations = [
        migrations.AlterField(
            model_name='appointment',
            name='vin',
            field=models.CharField(max_length=17),
        ),
        migrations.AlterField(
            model_name='automobilevo',
            name='vin',
            field=models.CharField(max_length=17, unique=True),
        ),
        migrations.AlterField(
            model_name='technician',
            name='employee_id',
            field=models.CharField(max_length=50, unique=True),
        ),
    ]
