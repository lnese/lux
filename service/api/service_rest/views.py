from django.http import JsonResponse
from .models import Technician, Appointment, AutomobileVO
from .encoders import TechnicianListEncoder, AppointmentListEncoder
from django.views.decorators.http import require_http_methods
import json


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        try:
            appointments = Appointment.objects.all()
            inventoryAutos = AutomobileVO.objects.all()
            for appointment in appointments:
                for auto in inventoryAutos:
                    if str(appointment) == str(auto):
                        appointment.updateVIP()
            return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder,
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "There are no appointments"},
                status=404,
            )
    else:
        content = json.loads(request.body)
        try:
            technician_id = content["technician"]
            technician = Technician.objects.get(id=technician_id)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid id"},
                status=404,
            )

        appointment = Appointment.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_show_appointment(request, pk):
    if request.method == "DELETE":
        try:
            count, _ = Appointment.objects.filter(id=pk).delete()
            updatedAppointments= Appointment.objects.all()
            return JsonResponse(
                {"deleted": count > 0, "appointments": updatedAppointments},
                encoder=AppointmentListEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                    {"message": "Invalid appointment id"},
                    status=404,
            )


@require_http_methods(["PUT"])
def api_finish_appointment(request, pk):
    try:
        appointment = Appointment.objects.get(id=pk)
        appointment.finish()
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
            return JsonResponse(
                    {"message": "Invalid appointment id"},
                    status=404,
            )

@require_http_methods(["PUT"])
def api_cancel_appointment(request, pk):
    try:
        appointment = Appointment.objects.get(id=pk)
        appointment.cancel()
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid appointment id"},
            status=404,
        )


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        try:
            technicians = Technician.objects.all()
            return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianListEncoder,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "There are no technicians"},
                status=404,
            )
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianListEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"message": "Could not create the technician"},
                status=404,
            )

@require_http_methods(["DELETE"])
def api_show_technician(request, pk):
    if request.method == "DELETE":
        try:
            count, _ = Technician.objects.filter(id=pk).delete()
            updatedTechnicians= Technician.objects.all()
            return JsonResponse(
                {"deleted": count > 0, "technicians": updatedTechnicians},
                encoder=TechnicianListEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                    {"message": "Invalid Technician id"},
                    status=404,
            )
