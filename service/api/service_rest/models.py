from django.db import models
from django.urls import reverse

class Status(models.Model):
    name = models.CharField(max_length=10)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("name",)
        verbose_name_plural = "statuses"


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin


class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=150)
    employee_id = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.employee_id

    def get_api_url(self):
        return reverse("api_show_technician", kwargs={"pk": self.id})


class Appointment(models.Model):

    @classmethod
    def create(cls, **kwargs):
        kwargs["status"] = Status.objects.get(name="created")
        appointment = cls(**kwargs)
        appointment.save()
        return appointment

    date_time = models.DateTimeField()
    reason = models.CharField(max_length=150)
    vin = models.CharField(max_length=17)
    vip = models.BooleanField(default=False)
    customer = models.CharField(max_length=200)
    status = models.ForeignKey(
        Status,
        related_name="appointments",
        on_delete=models.PROTECT,
    )
    technician = models.ForeignKey(
        Technician,
        related_name = "appointments",
        on_delete= models.CASCADE,
    )

    def cancel(self):
        status = Status.objects.get(name="canceled")
        self.status = status
        self.save()

    def finish(self):
        status = Status.objects.get(name="finished")
        self.status = status
        self.save()

    def updateVIP(self):
        self.vip = True
        self.save()

    def __str__(self):
        return self.vin

    def get_api_url(self):
        return reverse("api_show_appointment", kwargs={"pk": self.id})

    class Meta:
        ordering = ("date_time",)
